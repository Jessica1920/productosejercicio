$(document).ready(function () {


    
tabla();
    

});


function tabla()
{

    var url = 'obtenertabla';

    $.ajax({
        type: 'get',
        url: url,
        dataType: "JSON",
        success: function (vresponse) {

            
                    pintartabla(vresponse);
               
        },
        error: function (vjqXHR, vtextStatus, verrorThrown) {
            alert("errorhhh");
        }
    });
}


function  pintartabla(tabla) {

    var html = "";

    html += '<h1 class="big text-dark text-center" align="center">Solicitudes</h1>';
    html += '<br>';
    html += '<div class="table-responsive">';
    html += '<table class="table table-hover" id="tablas" >';
    html += '<thead style=" background: black; color: white;">';
    html += '<tr>';
    html += '<th style="text-align: center;">#</th>';
    html += '<th style="text-align: center;">NOMBRE</th>';
    html += '<th style="text-align: center;">PRESENTACION</th>';
    html += '<th style="text-align: center;">ACCIÓN</th>';
    html += '</tr>';
    html += '</thead>';
    html += '<tbody>';

    for (var i = 0; i < tabla.length; i++) {
         var numero = i + 1;
    html += '<tr class="info">';
    html += '<td style="text-align: center;">' + numero + '</td>';
    html += '<td style="text-align: center;">' + tabla[i].nombre + '</td>';
    html += '<td style="text-align: center;">' + tabla[i].presentacion + '</td>';
          

    html += '<td style="text-align: center;">';

    html += '<a class="btn btn-primary" href="ver/' + tabla[i].id + '"><span class="glyphicon glyphicon-eye-open"></span> Ver</a>';

    html += ' <a class="btn btn-warning" href="producto/' + tabla[i].id + '/edit"> <span class="glyphicon glyphicon-edit"></span></a>';

    html +=' <a class="btn btn-danger" href="eliminaproducto/' + tabla[i].id + '"><span class="glyphicon glyphicon-remove"></span></a>';
    html +='</td>';         
    
   
    html += '</tr>';
    }
    html += '</tbody>';
    html += '</table>';
    html += '</div>';
    $("#tabla").html(html);
    datatabletabla();

}




function datatabletabla() {
    $("#tablas").dataTable({
      

        "processing": true,
        "displayLength": 10,
        "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false}],

        "language": {
            "sSearch": "Buscar:",
            "sProcessing": "Procesando...",
            "sInfo": "_START_ - _END_ de _TOTAL_ Productos",
            "sInfoFiltered": "(filtrado de un total de _MAX_ solicitudes)",
            "sInfoPostFix": "",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",

            "oPaginate": {
                "sFirst": "Primero",

                "sLast": "ï¿½ltimo",

                "sNext": ">>",

                "sPrevious": "<<"
            },

            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

}
