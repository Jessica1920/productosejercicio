<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Redirect;
use \App\Model\Categorias;
use \App\Model\Productos;
use \App\Model\Presentaciones;
use \App\Model\Unidad_medida;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
    }

    public function tabla(){

            $productos = DB::table('productos')
      ->join('presentacion', 'productos.Fk_presentacion', '=', 'presentacion.id')

            ->get();
            
           // dd($productos);
            return json_encode($productos);
            }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categorias = Categorias::all();
        $presentacion = Presentaciones::all();
        $unidad_m = Unidad_medida::all();

        return view('producto.create', compact('categorias','presentacion','unidad_m'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


           $this->validate(request(),[
        'nombre' => 'required',
        'descripcion' => 'required',
        'cantidad' => 'required',
        'precio' => 'required',
        'presentacion' => 'required|not_in:0',
        'categoria' => 'required|not_in:0',
        'umedida' => 'required|not_in:0'
  
      ]);

       DB::table('productos')->insert([
            "nombre" =>  $request['nombre'],
            "descripcion" =>  $request['descripcion'],
            "cantidad" =>  $request['cantidad'],
            "precio" =>  $request['precio'],

            "Fk_presentacion" =>  $request['presentacion'],
            "Fk_categoria" =>  $request['categoria'],
            "Fk_umedida" =>  $request['umedida'],
           
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
            ]);
          return view('welcome');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto = DB::table('productos as pro')   
        ->join('categorias as catego', 'pro.Fk_categoria', '=', 'catego.id')
        ->join('presentacion as p', 'pro.Fk_presentacion', '=', 'p.id')
        ->join('unidad_medidas as um', 'pro.Fk_umedida', '=', 'um.id')
        ->where('pro.id', $id)
        ->first();
// dd($producto);
        return view('producto.show', compact('producto'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $producto = DB::table('productos')
       ->select('productos.id', 'productos.nombre','productos.descripcion','productos.cantidad', 'productos.precio', 'productos.Fk_presentacion', 'productos.Fk_categoria', 'productos.Fk_umedida','categorias.categorias','presentacion.presentacion','unidad_medidas.unidad_medidas')
       ->where('productos.id', $id)
      ->join('categorias', 'productos.Fk_categoria', '=', 'categorias.id')
      ->join('presentacion', 'productos.Fk_presentacion', '=', 'presentacion.id')
      ->join('unidad_medidas', 'productos.Fk_umedida', '=', 'unidad_medidas.id')
      ->first();



       // dd($producto);
        $categorias = Categorias::all();
        $presentacion = Presentaciones::all();
        $unidad_m = Unidad_medida::all();
        return view('producto.edit', compact('producto','categorias','presentacion', 'unidad_m'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function update(Request $request, $id)
    {
          $this->validate(request(),[
        'nombre' => 'required',
        'descripcion' => 'required',
        'cantidad' => 'required',
        'precio' => 'required',
        'presentacion' => 'required|not_in:0',
        'categoria' => 'required|not_in:0',
        'umedida' => 'required|not_in:0'
  
      ]);


      $proo =  DB::table('productos')->where('id',$id)->update([
            "nombre" =>  $request['nombre'],
            "descripcion" =>  $request['descripcion'],
            "cantidad" =>  $request['cantidad'],
            "precio" =>  $request['precio'],

            "Fk_presentacion" =>  $request['presentacion'],
            "Fk_categoria" =>  $request['categoria'],
            "Fk_umedida" =>  $request['umedida'],
           
            "updated_at" => Carbon::now(),

            ]);

// dd($proo);
   return redirect()->route('welcome');
 
// return "edit". $id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('productos')->where('id', $id)->delete();

    return redirect()->route('welcome');
        // return "fuer";
    }
}
