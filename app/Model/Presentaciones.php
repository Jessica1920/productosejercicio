<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Presentaciones extends Model
{
     protected $table = 'presentacion';
    protected $primaryKey = 'id';
    protected $fillable =['presentacion'];
}
