<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
      protected $table = 'productos';
    protected $primaryKey = 'id';
    protected $fillable =['nombre', 'descripcion', 'cantidad','precio', 'Fk_presentacion', 'Fk_categoria', 'Fk_umedida'];
   public function presentacion() {
        return $this->belongsTo('App\Model\Presentacion');
    }
    public function categoria() {
        return $this->belongsTo('App\Model\Categoria');
    }
    public function unidadmedida() {
        return $this->belongsTo('App\Model\Unidad_medida');
    }
}

 