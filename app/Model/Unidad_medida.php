<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Unidad_medida extends Model
{
     protected $table = 'unidad_medidas';
    protected $primaryKey = 'id';
    protected $fillable =['unidad_medidas'];
}
