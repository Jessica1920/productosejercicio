<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('obtenertabla','ProductoController@tabla');
Route::get('/',['as' => 'welcome', 'uses' => 'ProductoController@index'] );
Route::get('producto/create',['as' => 'producto.create', 'uses' => 'ProductoController@create'] );
Route::post('producto',['as' => 'producto.store', 'uses' => 'ProductoController@store'] );
Route::get('eliminaproducto/{id}','ProductoController@destroy');
Route::get('ver/{id}',['as' => 'producto.show', 'uses' => 'ProductoController@show'] );
Route::get('producto/{id}/edit',['as' => 'producto.edit', 'uses' => 'ProductoController@edit'] );
Route::put('producto/{id}',['as' => 'producto.update', 'uses' => 'ProductoController@update'] );
