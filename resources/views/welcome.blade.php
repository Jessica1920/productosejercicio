<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <title>Laravel</title>

        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
        
      

        <!-- jquery............................................................ -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>

       <script type="text/javascript" src="{{ asset ('js/producto.js') }}"></script>

        
        <link href="{{ asset ('datatables/css/jquery.datatables.min.css') }}" rel="stylesheet" type="text/css"/>
        <script src="{{ asset ('datatables/js/jquery.datatables.min.js') }}"></script>
        
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!-- fin jquery............................................................ -->
    <a href="{{asset('app.blade.php')}}"></a>
<!--<script src="{{ asset('js/jquery-3.3.1.min.js.js')}}"></script>-->
    <!-- Bootstrap ...........................................................................-->
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/bootstrap-theme.min.css') }}">
    <script type="text/javascript" src="{{ asset ('js/bootstrap.min.js') }}"></script>
    <!-- fin Bootstrap .........................................................................-->
    <!--CSS DE BOOTSTRAP-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <!--FIN DEL CSS DE BOOTSTRAP-->
    <!-- Fonts --><link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">              

    <style>
        .error{
            color: red;
        }

   
        body{
            background: #e7e1e1;
        }      


        #btt:before {
            content:'';
            position: absolute;
            top: 0px;
            left: 0px;
            width: 0px;
            height: 42px;
            background: rgba(255,255,255,0.3);
            border-radius: 5px;
            transition: all 2s ease;
        }        

        #btt:hover:before {
            width: 100%;
        }        

        /*oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo*/    
        .boton{
            padding: 10px 35px;
            overflow:hidden;
        }     
        .boton:before {
            font-family: FontAwesome;
            content:"\f07a";
            position: absolute;
            top: 11px;
            left: -30px;
            transition: all 200ms ease;
        }  
        .boton:hover:before {
            left: 7px;
        }


    </style>
</head>
<body>      

    <div class="content">

        <div class="container">                                   
            <br>
            <br> 

            <header>
                <nav class="navbar navbar-default navbar-inverse">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1">
                                <span class="sr-only">Menu</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a href="{{asset('/')}}" class="navbar-brand">Home</a>
                        </div>

                        <div class="collapse navbar-collapse" id="navbar-1">
                            <ul class="nav navbar-nav">
                               
                           </ul>


                        </div>     

                    </div>
                </nav>
            </header>

        </div>


        <div class="container">
           

<h1 align="center">Tienda de Abarrotes</h1>


<center>
  <a href="{{ route('producto.create') }}" class="btn btn-success" role="button">Registrar Nuevo Producto</a>
</center>

<hr>
<br>

<div id="tabla"></div>
           
        </div>



    </div>


</div>
</div>
</div>


</body>
</html>


