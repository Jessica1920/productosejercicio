@extends('layouts.app')

@section('content')

<h1 align="center">Nombre del producto: {{ $producto->nombre }}</h1>

<h3 align="center">Categoria: {{ $producto->categorias }}</h3>

<br>
<div align="center">
<label >Descripcion:</label>
<h3 align="center">{{ $producto->descripcion}}</h3>
</div>




<hr>
<h4 align="center"> Presentaciónes</h4>
<table align="center">
	<thead style=" background-color: black; color:white;">
                    <tr  align="center">
                        <td>Presentacíon</td>
                        <td>Cantidad</td>
                        <td>Unidad de Medida</td>
                        <td>Precio</td>
                    </tr>
                </thead>

                <tbody id="table">
                    <tr>
                        <td><input type="text" disabled value="{{$producto->presentacion}}"  class="form-control "></td>
                        <td><input type="text" disabled value="{{$producto->precio}}"  class="form-control "></td>
                        <td><input type="text" disabled value="{{$producto->cantidad}}"  class="form-control "></td>
                        <td><input type="text" disabled value="{{$producto->unidad_medidas}}"  class="form-control "></td>
                        
                    </tr>
                </tbody>
</table>
<hr>



<h5>Fecha de registro: {{$producto->created_at}}</h5>


@endsection