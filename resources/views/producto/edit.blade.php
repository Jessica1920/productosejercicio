@extends('layouts.app')

@section('content')

<h1 align="center">Tienda de Abarrotes</h1>
<h3 align="center">Editar</h3>

<hr>
<br>
<form method="POST" action="{{ route('producto.update',$producto->id) }}" >
    {!! method_field('PUT') !!}

 {!! csrf_field() !!}
 

    
   <br>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 text-center">
            <label>NOMBRE: </label>
            <input class="form-control text-center" value="{{$producto->nombre}}"   type="text" name="nombre" id="nombre">
            {!! $errors->first('nombre','<span class="error">:message</span>') !!}
        </div>
        <div class="col-md-3"></div>
    </div>
   <br>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 text-center">
        <label>Categoria: </label>
         <select class="form-control"   name="categoria" id="categoria">
         
         <option value="{{ $producto->Fk_categoria }}">{{ $producto->categorias }}</option>


          <option value="0">- Selecciona una opción -</option>
          

          @foreach ($categorias as $categoria)
          <option value="{{$categoria['id']}}">{{$categoria['categorias']}}</option>
          @endforeach



		 </select>
        {!! $errors->first('categoria','<span class="help-block">:message</span>') !!}


        </div>
        <div class="col-md-3"></div>
    </div>
   <br>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 text-center">
            <label>Descripcíon: </label>
            <textarea class="form-control"  name="descripcion" id="descripcion">{{$producto->descripcion}}</textarea>
        {!! $errors->first('descripcion','<span class="error">:message</span>') !!}
        </div>
        <div class="col-md-2"></div>
    </div>   
    <hr>
      
    <h4>Presentacíon del producto</h4>    
    
    <br>

            <div class="table-responsive">

            <table class="table table-bordered" >
                <thead style=" background-color: black; color: white;">
                    <tr  align="center">
                        <td>Presentacíon</td>
                        <td>Cantidad</td>
                        <td>Unidad de Medida</td>
                        <td>Precio</td>

                       
                    </tr>
                </thead>
                <tbody id="table">
                    <tr>

                       <td> <select class="form-control"   name="presentacion" id="presentacion">   
                      
                       <option value="{{ $producto->Fk_presentacion }}">{{ $producto->presentacion }}</option>

                         <option value="0">- Selecciona una opción -</option>


                          @foreach ($presentacion as $pre)

                          <option value="{{$pre['id']}}">{{$pre['presentacion']}}</option>
                          @endforeach 




                        </select>
                     {!! $errors->first('presentacion','<span class="help-block">:message</span>') !!}

                        </td>
                      <!-- oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
                        <td><input type="text"  id="cantidad" value="{{$producto->cantidad}}" name="cantidad" class="form-control">
                       {!! $errors->first('cantidad','<span class="error">:message</span>') !!}

                        </td>

                      <!-- oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->

                        <td> <select class="form-control"   name="umedida" id="umedida"> 
                         
                       <option value="{{ $producto->Fk_umedida }}">{{ $producto->unidad_medidas }}</option>

                          <option value="0">- Selecciona una opción -</option>
  
                           @foreach ($unidad_m as $medi)

                          <option value="{{$medi['id']}}">{{$medi['unidad_medidas']}}</option>
                          @endforeach   








                        </select>
                        {!! $errors->first('umedida','<span class="help-block">:message</span>') !!}
                        </td>
                      <!-- oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->

                        
                        <td><input type="text"  id="precio" value="{{$producto->precio}}" name="precio" class="form-control">
                        {!! $errors->first('precio','<span class="error">:message</span>') !!}
                        </td>
                      <!-- oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->


                    </tr>
                </tbody>

            </table>
                <!-- </div> -->
        </div>
















 
   
    <div  align="center">
      <input type="submit" class="btn btn-primary btn-lg"  value="Guardar">
    </div>
</form> 

 <br> <br>






@endsection
